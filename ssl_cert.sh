openssl pkey -in /etc/letsencrypt/live/logikally.com/privkey.pem -out ssl/server.key

openssl crl2pkcs7 -nocrl -certfile /etc/letsencrypt/live/logikally.com/fullchain.pem | openssl pkcs7 -print_certs -out ssl/server.cert
