# How to run XWiki
## Install
* Download enterprise version jar from https://www.xwiki.com/en/products/download-xwiki-enterprise
* Run the installer jar using "java -jar xwiki-blah-blah.jar". Follow the installer instructions.
* Once installed, go to the installed location (default: /Applications/XWiki Enterprise 8.4.4). 

## Pull existing data
* rm -rf data
* git clone git@bitbucket.org:sanjay51/xwiki.git
* mv xwiki/* .
* mv xwiki/.git .
* mv xwiki/.gitignore .

## Run XWiki
* /Applications/XWiki\ Enterprise\ 8.4.4/start_xwiki.sh

## Note
For any data changes, don't forget to commit new data to remote.

# Wonderor

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
