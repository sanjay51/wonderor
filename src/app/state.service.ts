import { EntityMetadata } from './entity/entityMetadata';
import { ROUTE_PROFILE, ROUTE_HOME, SidebarOption } from './sidebar/sidebar-option';
import { LoggingService } from './logging.service';
import { Utils } from './utils';
import { Constants } from './constants';
import { Injectable, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class StateService {
  public authState: AuthenticationState = new AuthenticationState();
  public userEntityMetadata: EntityMetadata[] = [];
  public entityMetadataMap = {};

  public sidebarSelection: number = 0;
  private searchBarActive = false;
  public searchBarValue = "";

  public getPageHeading(): string {
    return SidebarOption.getPageHeadingByURL(this.router.url);
  }

  @Output() keyboardEvent: EventEmitter<KeyboardEvent> = new EventEmitter();

  constructor(private router: Router, private logger: LoggingService) { }

  public getUserSub() {
    return this.authState.userSub;
  }

  public setSidebarSelection(selection) {
    console.log(this.sidebarSelection);
    this.sidebarSelection = selection;
  }

  public processKeyDown(event: KeyboardEvent) {

    let isKeyDownOnInputArea: boolean = Utils.isEventForInputArea(event);
    if (isKeyDownOnInputArea || event.keyCode == Constants.ESCAPE_KEY) {
      this.searchBarActive = false;
    } else if (Utils.isAlphanumericKeycode(event.keyCode)) {
      this.activateSearchBar(event.key);
    }

    this.keyboardEvent.emit(event);
  }

  public activateSearchBar(value: string) {
    if (!this.isSearchBarActive()) {
      this.searchBarActive = true;
      this.searchBarValue = value;
    }

    this.logger.debug(this.searchBarActive);
  }

  public deactivateSearchBar() {
    if (this.isSearchBarActive())
    this.searchBarActive = false;

    this.logger.debug(this.searchBarActive);
  }

  public isSearchBarActive() {
    return this.searchBarActive;
  }

  public navigate(to: string, id: string) {
    let link = ['/' + to]

    if (id) {
      link = ['/' + to, id]
    }
    
    this.router.navigate(link)
  }

  public navigateToProfile(id: string) {
    this.navigate(ROUTE_PROFILE, id);
  }
}

export class AuthenticationState {
  username: string;
  name: string;
  email: string;
  jwtToken: string;
  userSub: string;
}