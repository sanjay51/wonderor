import { Observable } from 'rxjs/internal/Observable';
import { API } from './api';
import { HttpClient, HttpParams } from '@angular/common/http';

export class Login extends API {
    public static API_NAME: string = "login";

    email: string;
    password: string;

    constructor(email: string, password: string) {
        super();
        this.email = email;
        this.password = password;
    }

    serve(http: HttpClient, URL: string): Observable<any> {
        let params = new HttpParams()
            .set("api", "login")
            .set("email", this.email)
            .set("password", this.password);

        return http.get(URL, {params: params});
    }

    validate() {
        if (!this.email || !this.password) {
            throw new Error("Login validation failed.");
        }
    }
}