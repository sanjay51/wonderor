import { Observable } from 'rxjs/internal/Observable';
import { API } from './api';
import { HttpClient, HttpParams } from '@angular/common/http';

export class SignUp extends API {
    public static API_NAME: string = "signup";

    email: string;
    password: string;
    fname: string;
    lname: string;

    constructor(email: string, password: string, 
        fname: string, lname: string) {
        super();
        this.email = email;
        this.password = password;
        this.fname = fname;
        this.lname = lname;
    }

    serve(http: HttpClient, URL: string): Observable<any> {
        let params = new HttpParams()
            .set("api", "signup")
            .set("email", this.email)
            .set("password", this.password)
            .set("fname", this.fname)
            .set("lname", this.lname);

        return http.get(URL, {params: params});
    }

    validate() {
        if (!this.email || !this.password 
            || !this.fname || !this.lname) {
            throw new Error("Sign up validation failed.");
        }
    }
}