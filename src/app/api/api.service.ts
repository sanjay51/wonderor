import { SignUp } from './signup';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Login } from './login';
import { API } from './api';

@Injectable({
  providedIn: 'root'
})
export class APIService {
  URL: string = "https://j979lj6fhb.execute-api.us-west-2.amazonaws.com/production";

  constructor(private http: HttpClient) { }

  public call(api: API) {
    return api.call(this.http, this.URL);
  }
}
