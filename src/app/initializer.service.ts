import { EntityDataService } from './entity/entity-data.service';
import { AuthenticationService } from './user/authentication.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class InitializerService {

  constructor(private authService: AuthenticationService, 
    private entityData: EntityDataService
  ) { }

  public initialize() {
    let isSignedIn = this.authService.tryLoadAuthStateFromLocalStorage();

    if (isSignedIn) {
      this.entityData.tryInitializeEntityMetadatumForCurrentUser();
    }
  }

  public initializeAfterLogin(user: any) {
    this.authService.populateAuthState(user)
    .then(response => {
      this.entityData.tryInitializeEntityMetadatumForCurrentUser();
    })
  }
}
