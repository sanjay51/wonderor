import { ELEMENT_SEARCH_BAR, ELEMENT_TOP_SEARCH_BAR } from '../constants';
import { StateService } from '../state.service';
import { Component, OnInit, HostListener, ElementRef, ViewChild } from '@angular/core';
import { ROUTE_SEARCH } from '../sidebar/sidebar-option';

@Component({
  selector: ELEMENT_SEARCH_BAR,
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {

  constructor(public stateService: StateService, private eRef: ElementRef) { }

  ngOnInit() {
  }

  @HostListener('document:click', ['$event']) onClick(event) {
    if (!this.stateService.isSearchBarActive()) {
      return;
    }

    let isInsideClick = false;

    for (var x in event.path) {
      if (isInsideClick) break;

      if (!x || !event.path[x] || !event.path[x].localName) continue;

      let localName = event.path[x].localName
      let isTopSearchBarButtonClick = (localName.indexOf(ELEMENT_TOP_SEARCH_BAR) != -1)
      let isSearchBarClick = localName.indexOf(ELEMENT_SEARCH_BAR) != -1

      isInsideClick = isTopSearchBarButtonClick || isSearchBarClick
    }

    let isClickOnSearchButton = false;
    if (event.target && event.target.innerText.toLowerCase() == ROUTE_SEARCH) {
      isClickOnSearchButton = true;
    }

    if (!(isInsideClick || isClickOnSearchButton)) this.stateService.deactivateSearchBar()
  }
}
