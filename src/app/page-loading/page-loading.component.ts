import { Component, OnInit } from '@angular/core';
import { Constants } from '../constants';

@Component({
  selector: 'app-page-loading',
  templateUrl: './page-loading.component.html',
  styleUrls: ['./page-loading.component.css']
})
export class PageLoadingComponent implements OnInit {
  public LOADING_GIF_SRC: string = Constants.LOADING_GIF_SRC;

  constructor() { }

  ngOnInit() {
  }

}
