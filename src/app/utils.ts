import { Entity } from "./entity/entity";

export class Utils {
    static isAlphanumericKeycode(keyCode: number): boolean {
        // ref https://www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes
        let isNumber = (keyCode >= 48 && keyCode <= 57);
        let isFromNumpad = (keyCode >= 96 && keyCode <= 105);
        let isCharacter = (keyCode >= 65 && keyCode <= 90);
        return isNumber || isFromNumpad || isCharacter;
    }

    static isEventForInputArea(event: KeyboardEvent) {
        let isEventForActualSearchBar = false;
        for (var x in event["path"]) {
            if (!x || !event["path"][x] || !event["path"][x].className) continue;
            if (event["path"][x].className.indexOf("searchbar") != -1) {
                isEventForActualSearchBar = true;
            }
        }

        if (!isEventForActualSearchBar && event.target
            && this.isEventTargetAnInputField(event.target)) {
            return true;
        }

        return false;
    }

    static isEventTargetAnInputField(target: any) {
        return target["localName"] == "input"
            || target["localName"] == "textarea"
    }

    static assertNotNull(target: any, label: string) {
        if (!target) throw new Error(label + " validation failed.");
    }

    static assertNotEmpty(target: any, label: string) {
        if ((!target) || (target.length == 0)) throw new Error(label + " validation failed.");
    }

    static sortEntitiesByCreatedEpoch(entities: Entity[]) {
        entities.sort((a, b) => a.createdEpoch > b.createdEpoch ? -1 : 1)
    }

    public static parseEpochToDate(epoch: number) {
      let todayEpoch = new Date().getTime()
      return Math.round(((todayEpoch - epoch)) / (24*60*60*1000))
    }
}