import { StateService } from './state.service';
import { Directive, Input, ElementRef } from '@angular/core';

@Directive({
  selector: '[focus]'
})
export class FocusDirective {
  @Input('focus') focus: boolean;

  constructor(private elem: ElementRef, private stateService: StateService) {
    this.stateService.keyboardEvent.subscribe(event => {
      if (this.focus) {
        elem.nativeElement.focus();
      }
    })
  }
}
