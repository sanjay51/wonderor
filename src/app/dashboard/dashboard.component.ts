import { Utils } from '../utils';
import { Component, OnInit } from '@angular/core';
import { EntityDataService } from '../entity/entity-data.service';
import { Entity } from '../entity/entity';
import { LoggingService } from '../logging.service';
import { Story } from '../entity/story';
import { StateService } from '../state.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public stories: Story[] = [];
  public error: string;
  public isPageLoading = true;

  constructor(public entityDataService: EntityDataService,
    public logger: LoggingService
  ) { }

  ngOnInit() {
    this.entityDataService.getPopularStories()
      .then(response => {
        console.log(response);

        this.stories = Story.fromPopularStories(response.response);
        console.log(this.stories);
        this.isPageLoading = false;
      })
      .catch(err => {
        this.logger.error(err)
        this.error = "Problem loading. Please refresh the page.";
      })
  }
}
