import { AddAnswerComponent } from './entity/add-answer/add-answer.component';
import { StoryComponent } from './entity/story/story.component';
import { LoginGuardService as LoginGuard } from './user/login/login-guard.service';
import { ProfileComponent } from './user/profile/profile.component';
import { AddQuestionComponent } from './entity/add-question/add-question.component';
import { AboutComponent } from './about/about.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ROUTE_HOME, ROUTE_ABOUT, ROUTE_ASK_A_QUESTION, ROUTE_PROFILE, ROUTE_LOGIN, ROUTE_SIGNUP, ROUTE_STORY, ROUTE_ANSWER } from './sidebar/sidebar-option';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import { LoginComponent } from './user/login/login.component';
import { SignupComponent } from './user/signup/signup.component';

const routes: Routes = [
  { path: '', redirectTo: ROUTE_HOME, pathMatch: 'full' },
  { path: ROUTE_HOME, component: DashboardComponent },
  { canActivate: [LoginGuard], path: ROUTE_ASK_A_QUESTION, component: AddQuestionComponent},
  { path: ROUTE_ABOUT, component: AboutComponent },
  { canActivate: [LoginGuard], path: ROUTE_PROFILE, component: ProfileComponent },
  { path: ROUTE_PROFILE + "/:profileId", component: ProfileComponent },
  { path: ROUTE_LOGIN, component: LoginComponent },
  { path: ROUTE_SIGNUP, component: SignupComponent },
  { path: ROUTE_STORY + "/:questionId", component: StoryComponent},
  { canActivate: [LoginGuard], path: ROUTE_ANSWER + "/:questionId", component: AddAnswerComponent}
]

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})
export class AppRoutingModule {
}