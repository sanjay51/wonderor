import { StoryComponent } from './entity/story/story.component';
import { APIService } from './api/api.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule }    from '@angular/forms';

import { AppComponent } from './root/app.component';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { StoryPreviewComponent } from './entity/story-preview/story-preview.component';
import { EntityDataService } from './entity/entity-data.service';
import { LoginComponent } from './user/login/login.component';
import { SignupComponent } from './user/signup/signup.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './user/profile/profile.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { AboutComponent } from './about/about.component';
import { ListElementDirective } from './sidebar/list-element.directive';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { FocusDirective } from './focus.directive';
import { HttpClientModule } from '@angular/common/http';
import { AmplifyAngularModule, AmplifyService } from 'aws-amplify-angular';
import { AddQuestionComponent } from './entity/add-question/add-question.component';
import { AddAnswerComponent } from './entity/add-answer/add-answer.component';
import { PageLoadingComponent } from './page-loading/page-loading.component';
import { MarkdownModule } from 'ngx-markdown';
import { SimplemdeModule, SIMPLEMDE_CONFIG } from 'ng2-simplemde'
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TopBarComponent } from './top-bar/top-bar.component';
import { TopSearchBarComponent } from './top-bar/top-search-bar/top-search-bar.component';
import { TopProfileMenuComponent } from './top-bar/top-profile-menu/top-profile-menu.component';
import { ProfileTab1Component } from './user/profile/profile-tab1/profile-tab1.component';
import { ProfileTab2Component } from './user/profile/profile-tab2/profile-tab2.component';
import { ProfileTab3Component } from './user/profile/profile-tab3/profile-tab3.component'

@NgModule({
  declarations: [
    AppComponent,
    StoryPreviewComponent,
    LoginComponent,
    SignupComponent,
    DashboardComponent,
    ProfileComponent,
    StoryComponent,
    SidebarComponent,
    AboutComponent,
    ListElementDirective,
    SearchBarComponent,
    FocusDirective,
    AddQuestionComponent,
    AddAnswerComponent,
    PageLoadingComponent,
    TopBarComponent,
    TopSearchBarComponent,
    TopProfileMenuComponent,
    ProfileTab1Component,
    ProfileTab2Component,
    ProfileTab3Component
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    AmplifyAngularModule,
    MarkdownModule.forRoot(),
    SimplemdeModule.forRoot({
      provide: SIMPLEMDE_CONFIG,
      useValue: { autosave: { enabled: false }, placeholder: 'placeholder' }
    }),
    FontAwesomeModule
  ],
  providers: [EntityDataService, APIService, AmplifyService],
  bootstrap: [AppComponent]
})

export class AppModule { }
