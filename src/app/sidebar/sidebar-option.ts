export const HOME = "HOME"
export const SEARCH = "SEARCH"
export const ASK_A_QUESTION = "ASK A QUESTION"
export const ABOUT = "ABOUT"
export const PROFILE = "PROFILE"
export const LOGOUT = "LOGOUT"
export const LOGIN = "LOGIN"
export const SIGN_UP = "SIGN UP"

export const ROUTE_HOME = "home";
export const ROUTE_SEARCH = "search";
export const ROUTE_LOGOUT = "logout";
export const ROUTE_ASK_A_QUESTION = "ask-a-question";
export const ROUTE_ABOUT = "about";
export const ROUTE_PROFILE = "profile";
export const ROUTE_LOGIN = "login";
export const ROUTE_SIGNUP = "sign-up";
export const ROUTE_STORY= "s";
export const ROUTE_ANSWER= "answer";

export class SidebarOption {
    urlName: string;
    alias: string;
    icon: string;
    type: SidebarOptionType;

    public static alwaysVisibleOptions = [];
    public static signedInOnlyOptions = [];
    public static notSignedInOnlyOptions = [];

    constructor(urlName: string, alias: string, type: SidebarOptionType) {
        this.urlName = urlName;
        this.alias = alias;
        this.type = type;
    }

    public static getPageHeadingByURL(url: string): string {
        switch(url) {
            case "/" + ROUTE_HOME: return "Top Feed";
            case "/" + ROUTE_PROFILE: return "Profile";
            case "/" + ROUTE_ABOUT: return "About";

            default: return "";
        }
    }

    public static getOptionsByType(optionType: SidebarOptionType) {
        if (optionType == SidebarOptionType.ALWAYS_VISIBLE) return SidebarOption.alwaysVisibleOptions;
        if (optionType == SidebarOptionType.SIGNED_IN_ONLY) return SidebarOption.signedInOnlyOptions;
        if (optionType == SidebarOptionType.NOT_SIGNED_IN) return SidebarOption.notSignedInOnlyOptions;

        return SidebarOption.alwaysVisibleOptions;
    }

    public static initializeOptions() {
        SidebarOption.alwaysVisibleOptions = [
            new SidebarOption(ROUTE_HOME, HOME, SidebarOptionType.ALWAYS_VISIBLE),
            new SidebarOption(ROUTE_ASK_A_QUESTION, ASK_A_QUESTION, SidebarOptionType.ALWAYS_VISIBLE),
            new SidebarOption(ROUTE_ABOUT, ABOUT, SidebarOptionType.ALWAYS_VISIBLE)
        ]

        SidebarOption.signedInOnlyOptions = [
            new SidebarOption(ROUTE_PROFILE, PROFILE, SidebarOptionType.ALWAYS_VISIBLE),
            new SidebarOption(ROUTE_LOGOUT, LOGOUT, SidebarOptionType.ALWAYS_VISIBLE),
        ]
        
        SidebarOption.notSignedInOnlyOptions = [
            new SidebarOption(ROUTE_LOGIN, LOGIN, SidebarOptionType.ALWAYS_VISIBLE),
            new SidebarOption(ROUTE_SIGNUP, SIGN_UP, SidebarOptionType.ALWAYS_VISIBLE),
        ]
    }
}

export enum SidebarOptionType {
    ALWAYS_VISIBLE,
    SIGNED_IN_ONLY,
    NOT_SIGNED_IN
}




