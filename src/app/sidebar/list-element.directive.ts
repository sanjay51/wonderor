import { Router } from '@angular/router';
import { Directive, Input, HostListener, ElementRef, OnInit, SimpleChanges, OnChanges } from '@angular/core';
import { SidebarOption, ROUTE_HOME } from './sidebar-option';

@Directive({
  selector: '[listElement]'
})
export class ListElementDirective implements OnInit, OnChanges {

  @Input('isSelected') isSelected: boolean;
  @Input('mouseOverColor') mouseoverColor: string;
  @Input('optionRef') optionRef: SidebarOption

  constructor(private el: ElementRef) {
    this.el.nativeElement.style.cursor = 'pointer'
    this.el.nativeElement.style.display='block'
    this.el.nativeElement.style.fontSize = '12px'
    this.el.nativeElement.style.fontWeight = 'normal'
    this.el.nativeElement.style.marginBottom = '8px'
    this.el.nativeElement.style.paddingLeft = '10px'
    this.el.nativeElement.style.fontFamily = 'Tahoma, Geneva, sans-serif'
    this.el.nativeElement.style.border = '0px solid blue';
  }

  ngOnInit(): void {
    this.el.nativeElement.style.color = this.getColor()
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.el.nativeElement.style.color = this.getColor();
    this.el.nativeElement.style.borderLeft = this.getBorderLeft();
    this.el.nativeElement.style.backgroundColor = this.getBackgroundColor();
  }

  @HostListener('mouseover') onMouseOver() {
    this.el.nativeElement.style.color = this.mouseoverColor;
  }

  @HostListener('mouseout') onMouseOut() {
    this.el.nativeElement.style.color = this.getColor();
  }

  getColor() {
    if (this.isSelected) {
      return 'black'
    }

    return 'grey'
  }

  getBorderLeft() {
    if (this.isSelected) {
      return '2px solid blue';
    }

    return '0px solid blue';
  }

  getBackgroundColor() {
    if (this.isSelected) {
      return 'rgb(250, 250, 255)';
    }

    return '';

  }
}
