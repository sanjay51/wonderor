import { AuthenticationService } from '../user/authentication.service';
import { Constants } from '../constants';
import { StateService } from '../state.service';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { SidebarOptionType, SidebarOption, ROUTE_SEARCH, ROUTE_LOGOUT, HOME, SEARCH, ASK_A_QUESTION, ABOUT, PROFILE, LOGOUT, LOGIN, ROUTE_HOME, SIGN_UP } from './sidebar-option';
import { faHome, faSearch, faQuestionCircle, faInfoCircle, faInfo, faQuestion, faUser, faSignOutAlt, faSignInAlt, faFileSignature } from '@fortawesome/free-solid-svg-icons';
import { LoggingService } from '../logging.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  @Input() public title: string;
  public clazz: number[] = [];

  public ALWAYS_VISIBLE = SidebarOptionType.ALWAYS_VISIBLE;
  public SIGNED_IN_ONLY = SidebarOptionType.SIGNED_IN_ONLY;
  public NOT_SIGNED_IN = SidebarOptionType.NOT_SIGNED_IN;

  constructor(public authentication: AuthenticationService, 
    public stateService: StateService,
    private router: Router,
    private logger: LoggingService
  ) { }

  ngOnInit() {
  }

  refreshSelectionState(urlName: string) {
  }

  navigate(to: string) {
    this.refreshSelectionState(to)

    if (to == ROUTE_SEARCH) {
      this.stateService.activateSearchBar("");
    } else if (to == ROUTE_LOGOUT) {
      this.authentication.logout()
    } else {
      this.stateService.navigate(to, null);
    }
  }

  public getOptions(optionType: SidebarOptionType): SidebarOption[] {
    return SidebarOption.getOptionsByType(optionType);
  }

  getIconByAlias(name: string) {
    switch (name) {
      case HOME: return faHome
      case SEARCH: return faSearch
      case ASK_A_QUESTION: return faQuestion
      case ABOUT: return faInfoCircle
      case PROFILE: return faUser
      case LOGOUT: return faSignOutAlt;
      case LOGIN: return faSignInAlt;
      case SIGN_UP: return faFileSignature;
    }
  }

  isSelected(option: SidebarOption) {
    if (this.router.url == '/' && option.urlName == ROUTE_HOME) {
      return true;
    }

    return this.router.url.endsWith(option.urlName)
  }
}
