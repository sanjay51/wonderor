import { InitializerService } from '../initializer.service';
import { StateService } from '../state.service';
import { EntityDataService } from '../entity/entity-data.service';
import { Component, HostListener, OnInit } from '@angular/core';
import Amplify from 'aws-amplify';
import awsmobile from './../../aws-exports';
import { SidebarOption } from '../sidebar/sidebar-option';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  public title = 'logikally';

  constructor(public stateService: StateService, 
    private initializer: InitializerService) {
  }

  ngOnInit() {
    SidebarOption.initializeOptions();
    Amplify.configure(awsmobile);
    this.initializer.initialize();
  }

  @HostListener('document:keydown', ['$event']) onKeyDown(event: KeyboardEvent) {
    this.stateService.processKeyDown(event);
  }
}
