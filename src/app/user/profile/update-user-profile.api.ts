import { Observable } from 'rxjs/internal/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import { API } from '../../api/api';
import { Utils } from '../../utils';

export class UpdateUserProfileAttributesAPI extends API {
    public static API_NAME: string = "updateUserProfileAttributes";

    userSub: string;
    attributes: Attribute[];

    constructor(userSub: string, attributes: Attribute[]) {
        super();
        this.userSub = userSub;
        this.attributes = attributes;
    }

    serve(http: HttpClient, URL: string): Observable<any> {
        let params = new HttpParams()
            .set("api", UpdateUserProfileAttributesAPI.API_NAME)
            .set("userSub", this.userSub)

        for (let att of this.attributes) {
            params = params.set(att.attributeName, att.attributeValue)
        }

        return http.get(URL, {params: params});
    }

    validate() {
        Utils.assertNotNull(this.attributes, "attributes");
        Utils.assertNotEmpty(this.userSub, "userSub");
    }
}

export class Attribute {
    attributeName: string;
    attributeValue: string;

    constructor(attName: string, attValue: string) {
        this.attributeName = attName;
        this.attributeValue = attValue;
    }
}