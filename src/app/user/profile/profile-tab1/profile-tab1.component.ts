import { UserProfileService } from './../user-profile.service';
import { ROUTE_STORY } from '../../../sidebar/sidebar-option';
import { Component, OnInit } from '@angular/core';
import { Entity } from '../../../entity/entity';
import { EntityDataService } from '../../../entity/entity-data.service';
import { StateService } from '../../../state.service';
import { LoggingService } from '../../../logging.service';
import { Utils } from '../../../utils';
import { ActivatedRoute } from '@angular/router';
import { UserProfile } from '../user-profile';

@Component({
  selector: 'app-profile-tab1',
  templateUrl: './profile-tab1.component.html',
  styleUrls: ['./profile-tab1.component.css']
})
export class ProfileTab1Component implements OnInit {
  public authorId;
  public authorUsername;
  public isCurrentUserProfile = true;

  public entities: Entity[] = [];
  public error: string;
  public isPageLoading = true;

  constructor(private activeRoute: ActivatedRoute, 
    private entityDataService: EntityDataService, 
    private state: StateService, 
    private userProfile: UserProfileService,
    private logger: LoggingService) { }

  ngOnInit() {
    this.authorId = this.activeRoute.snapshot.params.profileId;
    
    if (!this.authorId) {
      this.authorId = this.state.authState.userSub;
    }

    if (this.authorId != this.state.getUserSub()) {
      this.isCurrentUserProfile = false;
    }

    this.entityDataService.getEntitiesByAuthor(this.authorId)
      .then(response => {
        console.log(response);

        for (let obj of response.response) {
          this.entities.push(Entity.fromObject(obj));
        }

        Utils.sortEntitiesByCreatedEpoch(this.entities);

        this.isPageLoading = false;
      })
      .catch(err => {
        this.logger.error(err)
      })

      if (!this.isCurrentUserProfile) {
        this.userProfile.getUserProfile(this.authorId)
        .then(response => {
          let profile = UserProfile.fromObject(response.response);
          this.authorUsername = profile.username;
          console.log(this.authorUsername);
        })
        .catch(err => console.log(err))
      }
  }

  public parseEpochToDate(epoch: number) {
    return Utils.parseEpochToDate(epoch);
  }

  public gotoStory(entity: Entity) {
    this.state.navigate(ROUTE_STORY, entity.entityId);
  }
}
