import { Attribute } from './../update-user-profile.api';
import { AuthenticationService } from './../../authentication.service';
import { UserProfileService } from './../user-profile.service';
import { StateService } from '../../../state.service';
import { Component, OnInit } from '@angular/core';
import { UserProfile } from '../user-profile';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile-tab3',
  templateUrl: './profile-tab3.component.html',
  styleUrls: ['./profile-tab3.component.css']
})
export class ProfileTab3Component implements OnInit {
  public userProfile: UserProfile;

  public isPageLoading = true;
  public isUpdating = false;

  constructor(private activeRoute: ActivatedRoute, 
    public auth: AuthenticationService, public state: StateService, private profileService: UserProfileService) {
  }
  
  ngOnInit() {
    let userSub = this.state.getUserSub();

    this.profileService.getUserProfile(userSub)
    .then(response => {
      this.userProfile = UserProfile.fromObject(response.response);
      this.isPageLoading = false;
    })
    .catch(err => {
      console.log(err)
    })
  }

  public updateEmail() {
    let attCognitoName = "email"
    let attCognitoValue = this.userProfile.email;

    let attributes = [];
    attributes.push(new Attribute(attCognitoName, this.userProfile.email))

    this.updateAttribute(attCognitoName, attCognitoValue, attributes);
  }

  public updateName() {
    let attCognitoName = "name"
    let attCognitoValue = this.userProfile.getCognitoName();
    
    let attributes = [];
    attributes.push(new Attribute("fname", this.userProfile.fname))
    attributes.push(new Attribute("lname", this.userProfile.lname))

    this.updateAttribute(attCognitoName, attCognitoValue, attributes);
  }

  private updateAttribute(attCognitoName: string, 
                          attCognitoValue: string, 
                          attributes: Attribute[]) {

    this.isUpdating = true;
    let userSub = this.state.getUserSub();

    this.auth.getCognitoUser()
    .then(user => {
      this.auth.updateAttribute(user, attCognitoName, attCognitoValue)
      .then(response => {
        console.log(response)

        // save user profile in db
        this.profileService.updateUserProfileAttributes(userSub, attributes)
        .then(response => {
          console.log(response);
          this.isUpdating = false;
        })
      })
    })
  }
}