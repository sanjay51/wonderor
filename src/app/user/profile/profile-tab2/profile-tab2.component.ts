import { ROUTE_STORY } from '../../../sidebar/sidebar-option';
import { StateService } from '../../../state.service';
import { Component, OnInit } from '@angular/core';
import { Utils } from '../../../utils';

@Component({
  selector: 'app-profile-tab2',
  templateUrl: './profile-tab2.component.html',
  styleUrls: ['./profile-tab2.component.css']
})
export class ProfileTab2Component implements OnInit {

  constructor(private state: StateService) { }

  ngOnInit() {
  }

  public parseEpochToDate(epoch: number) {
    return Utils.parseEpochToDate(epoch);
  }

  public gotoStory(entityId: string) {
    this.state.navigate(ROUTE_STORY, entityId);
  }

}
