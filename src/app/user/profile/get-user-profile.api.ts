import { Observable } from 'rxjs/internal/Observable';
import { HttpClient, HttpParams } from '@angular/common/http';
import { API } from '../../api/api';
import { Utils } from '../../utils';

export class GetUserProfileAPI extends API {
    public static API_NAME: string = "getUserProfile";

    userSub: string;

    constructor(userSub: string) {
        super();
        this.userSub = userSub;
    }

    serve(http: HttpClient, URL: string): Observable<any> {
        let params = new HttpParams()
            .set("api", GetUserProfileAPI.API_NAME)
            .set("userSub", this.userSub)

        return http.get(URL, {params: params});
    }

    validate() {
        Utils.assertNotNull(this.userSub, "userSub");
    }
}