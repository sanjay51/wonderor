import { UserProfile } from './user-profile';
import { APIService } from './../../api/api.service';
import { Injectable } from '@angular/core';
import { GetUserProfileAPI } from './get-user-profile.api';
import { UpdateUserProfileAttributesAPI, Attribute } from './update-user-profile.api';

@Injectable({
  providedIn: 'root'
})
export class UserProfileService {

  constructor(private apiService: APIService) { }

  public getUserProfile(userSub: string): Promise<any> {
    let api: GetUserProfileAPI = new GetUserProfileAPI(userSub);
    return this.apiService.call(api).toPromise();
  }

  public updateUserProfileAttributes(userSub: string, attributes: Attribute[]): Promise<any> {
    let api: UpdateUserProfileAttributesAPI = 
        new UpdateUserProfileAttributesAPI(userSub, attributes);
    return this.apiService.call(api).toPromise();
  }
}
