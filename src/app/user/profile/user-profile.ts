import { HttpParams } from '@angular/common/http';
export class UserProfile {
    userSub: string;
    fname: string;
    lname: string;
    username: string;
    email: string;
    createdEpoch: number;
    lastUpdatedEpoch: number;

    public getCognitoName(): string {
        return this.fname + " " + this.lname;
    }

    public static fromObject(obj: any): UserProfile {
        let profile = new UserProfile();

        profile.userSub = obj.userSub;
        profile.fname = obj.fname;
        profile.lname = obj.lname;
        profile.username = obj.username;
        profile.email = obj.email;
        profile.createdEpoch = obj.createdEpoch;
        profile.lastUpdatedEpoch = obj.lastUpdatedEpoch;

        return profile;
    }

    public getAsHttpParams(): HttpParams {
        return new HttpParams()
        .set("userSub", this.userSub)
        .set("fname", this.fname)
        .set("lname", this.lname)
        .set("username", this.username)
        .set("email", this.email)
    }
}