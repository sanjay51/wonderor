import { LoggingService } from '../../logging.service';
import { StateService } from '../../state.service';
import { EntityDataService } from '../../entity/entity-data.service';
import { Component, OnInit } from '@angular/core';
import { Entity } from '../../entity/entity';
import { ROUTE_STORY } from '../../sidebar/sidebar-option';
import { Utils } from '../../utils';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  public profileId;
  public isCurrentUserProfile = true;

  constructor(private activeRoute: ActivatedRoute, 
    private entityDataService: EntityDataService, 
    private state: StateService, private logger: LoggingService) { }

  ngOnInit() {
    this.profileId = this.activeRoute.snapshot.params.profileId;

    if (this.profileId && this.profileId != this.state.getUserSub()) {
      this.isCurrentUserProfile = false;
    } else {
      this.isCurrentUserProfile = true;
    }
  }
}
