import { AuthenticationService } from '../authentication.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { ROUTE_LOGIN } from '../../sidebar/sidebar-option';

@Injectable({
  providedIn: 'root'
})
export class LoginGuardService implements CanActivate {

  constructor(private auth: AuthenticationService, private router: Router) { }

  canActivate(): boolean {
    if (!this.auth.isLoggedIn()) {
      this.router.navigate([ROUTE_LOGIN]);
      return false;
    }
    return true;
  }
}
