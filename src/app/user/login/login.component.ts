import { InitializerService } from '../../initializer.service';
import { EntityDataService } from '../../entity/entity-data.service';
import { Constants } from '../../constants';
import { ROUTE_HOME } from '../../sidebar/sidebar-option';
import { StateService } from '../../state.service';
import { APIService } from '../../api/api.service';
import { AuthenticationService } from '../authentication.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  returnUrl: string;
  LOADING_GIF_SRC: string = Constants.LOADING_GIF_SRC;

  constructor(
    private formBuilder: FormBuilder,
    public authentication: AuthenticationService,
    private state: StateService,
    private initalizer: InitializerService
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
        username: ['', Validators.required],
        password: ['', Validators.required]
    });

    this.authentication.logout();
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  onSubmit() {
      this.submitted = true;
      this.error = null;

      // stop here if form is invalid
      if (this.loginForm.invalid) {
          return;
      }

      this.loading = true;
      this.authentication.login(this.f.username.value, this.f.password.value)
        .then(user => {
          console.log(user);
          this.initalizer.initializeAfterLogin(user);
          this.state.navigate(ROUTE_HOME, null);
        })
        .catch(err => {
          console.log(err)
          this.error = err.message
        })
        .then(() => this.loading = false);
  }
}
