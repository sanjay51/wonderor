import { ROUTE_LOGIN } from '../../sidebar/sidebar-option';
import { SignupState } from './signupState';
import { ROUTE_HOME } from '../../sidebar/sidebar-option';
import { AuthenticationService } from '../authentication.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { StateService } from '../../state.service';
import { Constants } from '../../constants';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;

  constructor(private formBuilder: FormBuilder,
    public authentication: AuthenticationService,
    private state: StateService) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
        firstName: ['', Validators.required],
        lastName: ['', Validators.required],
        username: ['', Validators.required],
        email: ['', Validators.required],
        password: ['', [Validators.required, Validators.minLength(7)]]
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }

      this.loading = true;

      let signupState: SignupState = new SignupState(
        this.f.firstName.value,
        this.f.lastName.value,
        this.f.username.value,
        this.f.email.value,
        this.f.password.value,
      );
      
      this.authentication.signup(signupState)
        .then(user => {
          console.log(user);
          this.authentication.createUser(signupState, user.userSub);
          this.state.navigate(ROUTE_LOGIN, null);
        })
        .catch(err => {
          console.log(err)
          this.error = err.message
        })
        .then(() => this.loading = false);
  }

}
