import { Utils } from '../../utils';
import { HttpParams } from '@angular/common/http';
export class SignupState {
    fname: string;
    lname: string;
    username: string;
    email: string;
    password: string;

    constructor(fname: string, lname: string, username: string, email: string, password: string) {
        this.fname = fname;
        this.lname = lname;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    getAsHttpParams() {
        let params = new HttpParams()
        .set("fname", this.fname)
        .set("lname", this.lname)
        .set("username", this.username)
        .set("email", this.email)

        return params;
    }

    validate() {
        Utils.assertNotEmpty(this.fname, "first name");
        Utils.assertNotEmpty(this.lname, "last name");
        Utils.assertNotEmpty(this.username, "username");
        Utils.assertNotEmpty(this.email, "email");
        Utils.assertNotEmpty(this.password, "password");
    }
}