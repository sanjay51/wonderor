import { StateService, AuthenticationState } from '../state.service';
import { APIService } from '../api/api.service';
import { Injectable } from '@angular/core';
import { Auth } from 'aws-amplify';
import { SignupState } from './signup/signupState';
import { CreateUserAPI } from './create-user.api';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  public isLoggingIn: boolean = false;
  public isSigningUp: boolean = false;

  constructor(private apiService: APIService, private state: StateService) {
    this.tryLoadAuthStateFromLocalStorage();
   }

  public login(username: string, password: string) {
    this.isLoggingIn = true;

    return Auth.signIn(username, password)
  }

  public updateAttribute(user, attributeName: string, attributeValue): Promise<any> {
    let attribute = {}
    attribute[attributeName] = attributeValue;

    return Auth.updateUserAttributes(user, attribute);
  }

  public getCognitoUser(): Promise<any> {
    return Auth.currentAuthenticatedUser();
  }

  public profile() {
    return Auth.currentUserInfo();
  }

  public getAuthState() {
    return this.state.authState;
  }

  public signup(signupState: SignupState): Promise<any> {
    this.isSigningUp = true;

    return Auth.signUp({
      username: signupState.username,
      password: signupState.password,
      attributes: {
        email: signupState.email,
        name: signupState.fname + " " + signupState.lname
      }
    })
  }

  public createUser(signupState: SignupState, userSub: string) {
    let api: CreateUserAPI = new CreateUserAPI(signupState, userSub);
    this.apiService.call(api)
    .toPromise().catch(e => console.log(e));
  }

  public logout() {
    this.state.authState = new AuthenticationState();
    localStorage.clear();
  }

  public isLoggedIn() {
    return (this.state.authState.userSub != null);
  }

  public populateAuthState(user: any) {
    let jwtToken = user.signInUserSession.idToken.jwtToken;
    let userSub = user.signInUserSession.idToken.payload.sub;

    let promise = Auth.currentUserInfo();
    promise
    .then(user => {
      let authState = new AuthenticationState();
      authState.userSub = user.userSub;
      authState.username = user.username;
      authState.name = user.attributes.name;
      authState.email = user.attributes.email;
      authState.jwtToken = jwtToken;
      authState.userSub = userSub;

      this.state.authState = authState;
      this.saveAuthStateInLocalStorage();
    })

    return promise;
  }

  private saveAuthStateInLocalStorage() {
    localStorage.setItem("authState.username", this.state.authState.username);
    localStorage.setItem("authState.name", this.state.authState.name);
    localStorage.setItem("authState.email", this.state.authState.email);
    localStorage.setItem("authState.jwtToken", this.state.authState.jwtToken);
    localStorage.setItem("authState.userSub", this.state.authState.userSub);
  }

  public tryLoadAuthStateFromLocalStorage(): boolean {
    let userSub = localStorage.getItem("authState.userSub")

    if (userSub) {
      this.state.authState = new AuthenticationState();
      this.state.authState.username = localStorage.getItem("authState.username");
      this.state.authState.name = localStorage.getItem("authState.name");
      this.state.authState.email = localStorage.getItem("authState.email");
      this.state.authState.jwtToken = localStorage.getItem("authState.jwtToken");
      this.state.authState.userSub = localStorage.getItem("authState.userSub");

      return true;
    }

    return false;
  }
}