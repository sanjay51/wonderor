import { assertNotNull } from '@angular/compiler/src/output/output_ast';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { API } from '../api/api';
import { SignupState } from './signup/signupState';
import { Utils } from '../utils';

export class CreateUserAPI extends API {
    public static API_NAME: string = "createUser";

    signupState: SignupState;
    userSub: string;

    constructor(signupState: SignupState, userSub: string) {
        super();
        this.signupState = signupState;
        this.userSub = userSub;
    }

    serve(http: HttpClient, URL: string): Observable<any> {
        let params = this.signupState.getAsHttpParams()
            .set("api", CreateUserAPI.API_NAME)
            .set("userSub", this.userSub)

        return http.get(URL, {params: params});
    }

    validate() {
        Utils.assertNotNull(this.signupState, "Signup state");
        Utils.assertNotEmpty(this.userSub, "UserSub");
        this.signupState.validate();
    }
}