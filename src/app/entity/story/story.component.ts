import { ROUTE_ANSWER } from '../../sidebar/sidebar-option';
import { StateService } from '../../state.service';
import { EntityDataService } from '../entity-data.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Entity } from '../entity';
import { Constants } from '../../constants';

@Component({
  selector: 'app-story',
  templateUrl: './story.component.html',
  styleUrls: ['./story.component.css']
})
export class StoryComponent implements OnInit {
  public questionId: string;
  public question: Entity;
  public answers: Entity[] = [];

  public error: string;
  public isPageLoading = true;
  public areAnswersLoading = true;
  public LOADING_GIF_SRC: string = Constants.LOADING_GIF_SRC;

  constructor(private activeRoute: ActivatedRoute, 
    private entityData: EntityDataService,
    private state: StateService
  ) { }

  ngOnInit() {
    this.questionId = this.activeRoute.snapshot.params.questionId;

    this.entityData.getQuestionById(this.questionId)
    .then(dbObject => {
      console.log(dbObject)
      this.question = Entity.fromObject(dbObject.response)
      this.isPageLoading = false;
      console.log(this.question);
    })
    .catch(err => {
      console.log(err)
      this.error = err.message
    })

    this.entityData.getAnswers(this.questionId)
    .then(response => {
      for (let entityObj of response.response) {
        this.answers.push(Entity.fromObject(entityObj));
      }
      console.log(this.answers);
      this.areAnswersLoading = false;
    })
    .catch(err => {
      console.log(err);
    })
  }

  addAnswer() {
    this.state.navigate(ROUTE_ANSWER, this.questionId);
  }
}
