import { ROUTE_STORY } from '../../sidebar/sidebar-option';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Entity } from '../entity';
import { StateService } from '../../state.service';
import { EntityDataService } from '../entity-data.service';
import { ActivatedRoute } from '@angular/router';
import { Constants } from '../../constants';

@Component({
  selector: 'app-add-answer',
  templateUrl: './add-answer.component.html',
  styleUrls: ['./add-answer.component.css']
})
export class AddAnswerComponent implements OnInit {
  addAnswerForm: FormGroup;
  isSubmitting = false;
  submitted = false;
  error: string;
  
  public LOADING_GIF_SRC: string = Constants.LOADING_GIF_SRC;
  public isPageLoading = true;
  
  public questionId: string;
  public question: Entity;

  constructor(
    private formBuilder: FormBuilder,
    private activeRoute: ActivatedRoute, 
    private entityData: EntityDataService,
    private state: StateService
  ) { }

  ngOnInit() {
    this.questionId = this.activeRoute.snapshot.params.questionId;

    this.addAnswerForm = this.formBuilder.group({
        answerTitle: [''],
        answer: ['', Validators.required]
    });

    this.entityData.getQuestionById(this.questionId)
    .then(dbObject => {
      console.log(dbObject)
      this.question = Entity.fromObject(dbObject.response)
      this.isPageLoading = false;
      console.log(this.question);
    })
    .catch(err => {
      console.log(err)
      this.error = err.message
    })
  }

  // convenience getter for easy access to form fields
  get f() { return this.addAnswerForm.controls; }

  onSubmit() {

    // stop here if form is invalid
    if (this.addAnswerForm.invalid) {
        return;
    }

    this.isSubmitting = true;
    this.submitted = true;
    
    this.entityData.createAnswer(this.questionId, 
      (this.f.answerTitle.value.replace(/;/g, ',')), 
      (this.f.answer.value.replace(/;/g, ',')))
    .then(answer => {
      console.log(answer);
      this.state.navigate(ROUTE_STORY, this.questionId);
    })
    .catch(err => {
      console.log(err)
      this.error = err.message
    })
    .then(() => this.isSubmitting = false);
  }
}
