import { Observable } from 'rxjs/internal/Observable';
import { API } from '../../api/api';
import { HttpClient, HttpParams } from '@angular/common/http';
import { assertNotNull } from '@angular/compiler/src/output/output_ast';
import { Utils } from '../../utils';

export class AddAnswerAPI extends API {
    public static API_NAME: string = "addAnswer";

    questionId: string;
    authorId: string;
    title: string;
    content: string;
    type: string;

    constructor(questionId: string, authorId: string, title: string, 
        content: string) {
        super();
        this.questionId = questionId;
        this.authorId = authorId;
        this.title = title;
        this.content = content;
        this.type = "answer";
    }

    serve(http: HttpClient, URL: string): Observable<any> {
        let params = new HttpParams()
            .set("api", "createEntity")
            .set("entityId", this.questionId)
            .set("authorId", this.authorId)
            .set("title", this.title)
            .set("content", this.content)
            .set("type", this.type)

        return http.get(URL, {params: params});
    }

    validate() {
        Utils.assertNotNull(this.questionId, "questionId");
        Utils.assertNotNull(this.authorId, "authorId");
        Utils.assertNotNull(this.content, "content");
    }
}