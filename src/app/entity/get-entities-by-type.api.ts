import { Utils } from '../utils';
import { assertNotNull } from '@angular/compiler/src/output/output_ast';
import { Observable } from 'rxjs/internal/Observable';
import { API } from '../api/api';
import { HttpClient, HttpParams } from '@angular/common/http';

export class GetEntitiesByType extends API {
    public static API_NAME: string = "getEntitiesByType";

    type: string;

    constructor(type: string) {
        super();
        this.type = type;
    }

    serve(http: HttpClient, URL: string): Observable<any> {
        let params = new HttpParams()
            .set("api", GetEntitiesByType.API_NAME)
            .set("type", this.type)

        return http.get(URL, {params: params});
    }

    validate() {
        Utils.assertNotNull(this.type, "type")
    }
}