import { ROUTE_STORY } from '../../sidebar/sidebar-option';
import { StateService } from '../../state.service';
import { EntityDataService } from '../entity-data.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Constants } from '../../constants';

@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.css']
})
export class AddQuestionComponent implements OnInit {
  addQuestionForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  
  LOADING_GIF_SRC: string = Constants.LOADING_GIF_SRC;

  constructor(
    private formBuilder: FormBuilder,
    private entityDataService: EntityDataService,
    private state: StateService
  ) { }

  ngOnInit() {
    this.addQuestionForm = this.formBuilder.group({
        questionTitle: ['', Validators.required],
        questionDetails: ['']
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.addQuestionForm.controls; }

  onSubmit() {

    // stop here if form is invalid
    if (this.addQuestionForm.invalid) {
        return;
    }

    this.loading = true;
    this.submitted = true;
    this.entityDataService.createQuestion(this.f.questionTitle.value, this.f.questionDetails.value)
    .then(response => {
      console.log(response);
      this.state.navigate(ROUTE_STORY, response.response.entityId);
    })
    .catch(err => {
      console.log(err)
      this.error = err.message
    })
    .then(() => this.loading = false);
  }
}
