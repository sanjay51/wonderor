import { Observable } from 'rxjs/internal/Observable';
import { API } from '../../api/api';
import { HttpClient, HttpParams } from '@angular/common/http';

export class AddQuestionAPI extends API {
    public static API_NAME: string = "addQuestion";

    authorId: string;
    title: string;
    content: string;
    type: string;

    constructor(authorId: string, title: string, 
        content: string) {
        super();
        this.authorId = authorId;
        this.title = title;
        this.content = content;
        this.type = "question";
    }

    serve(http: HttpClient, URL: string): Observable<any> {
        let params = new HttpParams()
            .set("api", "createEntity")
            .set("authorId", this.authorId)
            .set("title", this.title)
            .set("content", this.content)
            .set("type", this.type)

        return http.get(URL, {params: params});
    }

    validate() {
        if (!this.authorId || !this.title) {
            throw new Error("Question validation failed.");
        }
    }
}