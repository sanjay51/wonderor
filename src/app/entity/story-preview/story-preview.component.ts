import { EntityDataService } from '../entity-data.service';
import { faThumbsUp as fasThumbsUp, faThumbsDown as fasThumbsDown } from '@fortawesome/free-solid-svg-icons';
import { ROUTE_STORY, ROUTE_ANSWER } from '../../sidebar/sidebar-option';
import { StateService } from '../../state.service';
import { Component, OnInit, Input } from '@angular/core';
import { Entity } from '../entity';
import { Story } from '../story';
import { faThumbsDown as farThumbsDown, faThumbsUp as farThumbsUp } from '@fortawesome/free-regular-svg-icons'

@Component({
  selector: 'app-story-preview',
  templateUrl: './story-preview.component.html',
  styleUrls: ['./story-preview.component.css']
})
export class StoryPreviewComponent implements OnInit {
  @Input() story: Story;
  fasThumbsUp = fasThumbsUp;
  farThumbsUp = farThumbsUp;
  fasThumbsDown = fasThumbsDown;
  farThumbsDown = farThumbsDown;

  constructor(private state: StateService, private entityDataService: EntityDataService) { }

  ngOnInit() {
  }

  public navigateToStory() {
    this.state.navigate(ROUTE_STORY, this.story.question.entityId);
  }

  addAnswer() {
    this.state.navigate(ROUTE_ANSWER, this.story.question.entityId);
  }

  navigateToProfile(id: string) {
    this.state.navigateToProfile(id);
  }

  public upvote(entity: Entity) {
    this.entityDataService.addEntityVote(entity.getActualEntityId(),
        entity.getParentEntityId(), entity.type, "1")
    .then(response => {
      console.log(response);
    })
    .catch(err => {
      console.log(err);
    })
  }

  public downvote(entity: Entity) {
    this.entityDataService.addEntityVote(entity.getActualEntityId(), 
        entity.getParentEntityId(), entity.type, "-1")
    .then(response => {
      console.log(response);
    })
    .catch(err => {
      console.log(err);
    })
  }

  public neutralVote(entity: Entity) {
    this.entityDataService.removeEntityVote(entity.getActualEntityId());
  }

  public getUpvoteCount(entity: Entity) {
    return entity.upvoteCount + (this.getUserVote(entity) == 1 ? 1 : 0)
  }

  public getDownvoteCount(entity: Entity) {
    return entity.downvoteCount + (this.getUserVote(entity) == -1 ? 1 : 0)
  }

  public getUserVote(entity: Entity) {
    let entityMetadata = this.state.entityMetadataMap[entity.getActualEntityId()]
    let userVote = 0
    if (entityMetadata) {
      userVote = +entityMetadata.value
    }

    return userVote;
  }

}
