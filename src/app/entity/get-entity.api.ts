import { assertNotNull } from '@angular/compiler/src/output/output_ast';
import { Observable } from 'rxjs/internal/Observable';
import { API } from '../api/api';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Utils } from '../utils';

export class GetEntityByIdAPI extends API {
    public static API_NAME: string = "getEntityById";

    entityId: string;
    type: string;
    subEntityId: string;

    constructor(entityId: string, type: string, subEntityId: string) {
        super();
        this.entityId = entityId;
        this.type = type;
        this.subEntityId = subEntityId;
    }

    serve(http: HttpClient, URL: string): Observable<any> {
        let params = new HttpParams()
            .set("api", GetEntityByIdAPI.API_NAME)
            .set("entityId", this.entityId)
            .set("type", this.type)
            .set("subEntityId", this.subEntityId)

        return http.get(URL, {params: params});
    }

    validate() {
        Utils.assertNotNull(this.entityId, "EntityId");
        Utils.assertNotNull(this.type, "type");
        if (!this.entityId || !this.type) {
            throw new Error("EntityId/type Validation failed.");
        }
    }
}