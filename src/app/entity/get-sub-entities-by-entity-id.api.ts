import { Utils } from '../utils';
import { assertNotNull } from '@angular/compiler/src/output/output_ast';
import { Observable } from 'rxjs/internal/Observable';
import { API } from '../api/api';
import { HttpClient, HttpParams } from '@angular/common/http';

export class GetSubEntitiesByEntityId extends API {
    public static API_NAME: string = "getSubEntitiesByEntityId";

    type: string;
    entityId: string;

    constructor(entityId: string, type: string) {
        super();
        this.type = type;
        this.entityId = entityId;
    }

    serve(http: HttpClient, URL: string): Observable<any> {
        let params = new HttpParams()
            .set("api", GetSubEntitiesByEntityId.API_NAME)
            .set("entityId", this.entityId)
            .set("type", this.type)

        return http.get(URL, {params: params});
    }

    validate() {
        Utils.assertNotNull(this.type, "type")
    }
}