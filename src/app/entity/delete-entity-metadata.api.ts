import { assertNotNull } from '@angular/compiler/src/output/output_ast';
import { Observable } from 'rxjs/internal/Observable';
import { API } from '../api/api';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Utils } from '../utils';

export class DeleteEntityMetadataAPI extends API {
    public static API_NAME: string = "deleteEntityMetadata";

    entityId: string;
    type: string;
    authorId: string;

    constructor(entityId: string, type: string, authorId: string) {
        super();
        this.entityId = entityId;
        this.type = type;
        this.authorId = authorId;
    }

    serve(http: HttpClient, URL: string): Observable<any> {
        let params = new HttpParams()
            .set("api", DeleteEntityMetadataAPI.API_NAME)
            .set("entityId", this.entityId)
            .set("type", this.type)
            .set("authorId", this.authorId)

        return http.get(URL, {params: params});
    }

    validate() {
        Utils.assertNotEmpty(this.entityId, "EntityId");
        Utils.assertNotEmpty(this.type, "type");
        Utils.assertNotEmpty(this.authorId, "authorId");
    }
}