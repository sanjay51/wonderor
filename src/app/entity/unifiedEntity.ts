import { Constants } from "../constants";

export class UnifiedEntity {
    entityId: string;
    tsId: string;
    type: string;
    authorId: string;
    title: string;
    content: string;
    upvoteCount: number;
    downvoteCount: number;
    rating: number;
    username: string;
    authorImage: string;
    authorBio: number;
    createdEpoch: string;
    lastUpdatedEpoch: number;

    constructor(entityId: string, title: string, content: string) {
        this.entityId = entityId;
        this.title = title;
        this.content = content;
    }

    public getActualEntityId() {
        // if this object is for answer, the entityId is actually not that of 
        // that answer, but it's question (or primary entity). This method
        // will return the actual entity id.

        if (this.getSubEntityId() != "") {
            return this.getSubEntityId();
        }

        return this.entityId
    }

    public getParentEntityId(): string {
        // if question => ""
        // if answer => return questionId

        if (this.type == Constants.ANSWER_TYPE) {
            return this.entityId;
        }

        return "null";
    }

    public getSubEntityId(): string {
        if (this.type == Constants.QUESTION_TYPE) {
            return "";
        } else {
            return this.tsId.split("_")[1];
        }
    }

    public static fromObject(obj: any): UnifiedEntity {
        let unifiedEntity = new UnifiedEntity(obj.entityId, obj.title, obj.content);
        unifiedEntity.entityId = obj.entityId;
        unifiedEntity.tsId = obj.tsId;
        unifiedEntity.type = obj.type;
        unifiedEntity.authorId = obj.authorId;
        unifiedEntity.title = obj.title;
        unifiedEntity.content = obj.content;
        unifiedEntity.upvoteCount = obj.upvoteCount;
        unifiedEntity.downvoteCount = obj.downvoteCount;
        unifiedEntity.rating = obj.rating;
        unifiedEntity.username = obj.username;
        unifiedEntity.authorImage = obj.authorImage;
        unifiedEntity.authorBio = obj.authorBio;
        unifiedEntity.createdEpoch = obj.createdEpoch;
        unifiedEntity.lastUpdatedEpoch = obj.lastUpdatedEpoch;

        return unifiedEntity;
    }
}