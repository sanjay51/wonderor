import { assertNotNull } from '@angular/compiler/src/output/output_ast';
import { Observable } from 'rxjs/internal/Observable';
import { API } from '../api/api';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Utils } from '../utils';

export class CreateEntityMetadataAPI extends API {
    public static API_NAME: string = "createEntityMetadata";

    entityId: string;
    parentEntityId: string;
    entityType: string;
    type: string;
    authorId: string;
    value: string;

    constructor(entityId: string, parentEntityId: string, entityType: string, type: string, authorId: string, value: string) {
        super();
        this.entityId = entityId;
        this.parentEntityId = parentEntityId;
        this.entityType = entityType;
        this.type = type;
        this.authorId = authorId;
        this.value = value;
    }

    serve(http: HttpClient, URL: string): Observable<any> {
        let params = new HttpParams()
            .set("api", CreateEntityMetadataAPI.API_NAME)
            .set("entityId", this.entityId)
            .set("parentEntityId", this.parentEntityId)
            .set("entityType", this.entityType)
            .set("type", this.type)
            .set("authorId", this.authorId)
            .set("value", this.value)

        return http.get(URL, {params: params});
    }

    validate() {
        Utils.assertNotEmpty(this.entityId, "EntityId");
        Utils.assertNotEmpty(this.type, "type");
        Utils.assertNotEmpty(this.authorId, "authorId");
        Utils.assertNotEmpty(this.value, "value");
    }
}