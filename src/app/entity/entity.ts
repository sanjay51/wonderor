import { Constants } from '../constants';

export class Entity {
    type: string;
    tsId: string;
    authorId: string;
    content: string;
    createdEpoch: number;
    downvoteCount: number;
    entityId: string;
    lastUpdatedEpoch: number;
    title: string;
    topics: string[];
    upvoteCount: number;

    constructor(entityId: string, title: string, content: string) {
        this.entityId = entityId;
        this.title = title;
        this.content = content;
    }

    public getActualEntityId() {
        // if this object is for answer, the entityId is actually not that of 
        // that answer, but it's question (or primary entity). This method
        // will return the actual entity id.

        if (this.getSubEntityId() != "") {
            return this.getSubEntityId();
        }

        return this.entityId
    }

    public getParentEntityId(): string {
        // if question => ""
        // if answer => return questionId

        if (this.type == Constants.ANSWER_TYPE) {
            return this.entityId;
        }

        return "null";
    }

    public getSubEntityId(): string {
        if (this.type == Constants.QUESTION_TYPE) {
            return "";
        } else {
            return this.tsId.split("_")[1];
        }
    }

    public static fromObject(obj: any): Entity {
        let entity = new Entity(obj.entityId, obj.title, obj.content);
        entity.authorId = obj.authorId;
        entity.type = obj.type;
        entity.tsId = obj.tsId;
        entity.createdEpoch = obj.createdEpoch;
        entity.downvoteCount = obj.downvoteCount;
        entity.lastUpdatedEpoch = obj.lastUpdatedEpoch;
        entity.topics = obj.topics;
        entity.upvoteCount = obj.upvoteCount;

        return entity;
    }
}