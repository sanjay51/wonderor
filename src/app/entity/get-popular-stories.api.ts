import { assertNotNull } from '@angular/compiler/src/output/output_ast';
import { Observable } from 'rxjs/internal/Observable';
import { API } from '../api/api';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Utils } from '../utils';

export class GetPopularStoriesAPI extends API {
    public static API_NAME: string = "getPopularStories";

    constructor() {
        super();
    }

    serve(http: HttpClient, URL: string): Observable<any> {
        let params = new HttpParams()
            .set("api", GetPopularStoriesAPI.API_NAME)

        return http.get(URL, {params: params});
    }

    validate() {
        // none
    }
}