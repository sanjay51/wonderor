import { GetEntitiesByAuthorAPI } from './get-entities-by-author.api';
import { EntityMetadata } from './entityMetadata';
import { StateService } from '../state.service';
import { GetEntityMetadatumByAuthorAPI } from './get-entity-metadatum-by-author.api';
import { DeleteEntityMetadataAPI } from './delete-entity-metadata.api';
import { GetPopularStoriesAPI } from './get-popular-stories.api';
import { GetSubEntitiesByEntityId } from './get-sub-entities-by-entity-id.api';
import { AddAnswerAPI } from './add-answer/add-answer.api';
import { AddQuestionAPI } from './add-question/add-question.api';
import { APIService } from '../api/api.service';
import { Injectable } from '@angular/core';
import { GetEntityByIdAPI } from './get-entity.api';
import { GetEntitiesByType } from './get-entities-by-type.api';
import { Constants } from '../constants';
import { CreateEntityMetadataAPI } from './create-entity-metadata.api';
import { LoggingService } from '../logging.service';

@Injectable({
  providedIn: 'root'
})
export class EntityDataService {

  constructor(private apiService: APIService,
    private state: StateService,
    private logger: LoggingService
  ) { }

  public createQuestion(questionTitle: string, questionDetails: string): Promise<any> {
    let authorId = this.state.getUserSub()
    let api: AddQuestionAPI = new AddQuestionAPI(authorId, questionTitle, questionDetails);

    return this.apiService.call(api).toPromise();
  }

  public createAnswer(questionId: string, answerTitle: string, answerContent: string): Promise<any> {
    let authorId = this.state.getUserSub()
    let api: AddAnswerAPI = new AddAnswerAPI(questionId, authorId, answerTitle, answerContent);

    return this.apiService.call(api).toPromise();
  }

  public getQuestionById(entityId: string): Promise<any> {
    let api: GetEntityByIdAPI = new GetEntityByIdAPI(entityId, "question", null);
    return this.apiService.call(api).toPromise();
  }

  public getQuestions(): Promise<any> {
    let api: GetEntitiesByType = new GetEntitiesByType(Constants.QUESTION_TYPE);
    return this.apiService.call(api).toPromise();
  }

  public getPopularStories(): Promise<any> {
    let api: GetPopularStoriesAPI = new GetPopularStoriesAPI();
    return this.apiService.call(api).toPromise();
  }

  public getEntitiesByAuthor(authorId: string): Promise<any> {
    let api: GetEntitiesByAuthorAPI = new GetEntitiesByAuthorAPI(authorId);
    return this.apiService.call(api).toPromise();
  }

  public getAnswers(questionId: string): Promise<any> {
    let api = new GetSubEntitiesByEntityId(questionId, Constants.ANSWER_TYPE);
    return this.apiService.call(api).toPromise();
  }

  public addEntityVote(entityId: string, parentEntityId: string, entityType: string, value: string): Promise<any> {
    let authorId = this.state.getUserSub()
    let type = "vote"
    let api = new CreateEntityMetadataAPI(entityId, parentEntityId, entityType, type, authorId, value);

    if (this.state.entityMetadataMap[entityId]) {
      this.state.entityMetadataMap[entityId].value = value;
    } else {
      let date = new Date()
      let entityMetadata = EntityMetadata.fromEntityMetadataObject({
        entityId: entityId,
        authorId: authorId,
        createdEpoch: date.getTime(), // best effort assignment
        lastUpdatedEpoch: date.getTime(), // best effort assignment
        type: "vote",
        value: value,
      })
      console.log(entityMetadata)
      this.state.entityMetadataMap[entityId] = entityMetadata;
    }
    return this.apiService.call(api).toPromise();
  }

  public removeEntityVote(entityId: string): Promise<any> {
    let authorId = this.state.getUserSub()
    let type = "vote"
    let api = new DeleteEntityMetadataAPI(entityId, type, authorId);

    if (this.state.entityMetadataMap[entityId]) {
      this.state.entityMetadataMap[entityId].value = "0";
    }

    return this.apiService.call(api).toPromise();
  }

  public getEntityMetadatumForCurrentUser(): Promise<any> {
    let authorId = this.state.getUserSub()
    let api = new GetEntityMetadatumByAuthorAPI(authorId);

    return this.apiService.call(api).toPromise();
  }

  public tryInitializeEntityMetadatumForCurrentUser() {
    this.getEntityMetadatumForCurrentUser()
      .then(response => {
        this.state.userEntityMetadata =
          EntityMetadata.fromEntityMetadatumObject(response.response);

        for (let entityMetadata of this.state.userEntityMetadata) {
          this.state.entityMetadataMap[entityMetadata.entityId] = entityMetadata
        }

        this.logger.info("User entity metadata initialized");
      })
      .catch(err => this.logger.warn("Failed to load user metadata. " + err))
  }
}
