import { UnifiedEntity } from './unifiedEntity';

export class Story {
    question: UnifiedEntity;
    answer: UnifiedEntity;

    constructor(question: UnifiedEntity, answer: UnifiedEntity) {
        this.question = question;
        this.answer = answer;
    }

    public static fromPopularStories(storyObjects: any): Story[] {
        let stories: Story[] = [];

        for (let storyObj of storyObjects) {
            let question: UnifiedEntity = UnifiedEntity.fromObject(storyObj.question);
            let answer: UnifiedEntity = UnifiedEntity.fromObject(storyObj.answer);

            stories.push(new Story(question, answer));
        }

        return stories;
    }
}