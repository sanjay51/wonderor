export class EntityMetadata {
    entityId: string;
    private parentEntityId: string;
    entityType: string;
    authorId: string;
    createdEpoch: number;
    lastUpdateEpoch: number;
    type: string;
    value: string;

    public static fromEntityMetadataObject(entityMetadataObject: any): EntityMetadata {
        let entityMetadata: EntityMetadata = new EntityMetadata();

        entityMetadata.entityId = entityMetadataObject.entityId;
        entityMetadata.parentEntityId = entityMetadataObject.parentEntityId;
        entityMetadata.entityType = entityMetadataObject.entityType;
        entityMetadata.authorId = entityMetadataObject.authorId;
        entityMetadata.createdEpoch = entityMetadataObject.createdEpoch;
        entityMetadata.lastUpdateEpoch = entityMetadataObject.lastUpdatedEpoch;
        entityMetadata.type = entityMetadataObject.type;
        entityMetadata.value = entityMetadataObject.value;

        return entityMetadata;
    }

    public getParentEntityId() {
        if (this.parentEntityId != "null") {
            return this.parentEntityId;
        }

        return this.entityId;
    }

    public static fromEntityMetadatumObject(entityMetadatumObjects: any): EntityMetadata[] {
        let entityMetadataList: EntityMetadata[] = [];

        for (let entityMetadataObj of entityMetadatumObjects) {
            let entityMetadata: EntityMetadata = EntityMetadata.fromEntityMetadataObject(entityMetadataObj);

            entityMetadataList.push(entityMetadata);
        }

        return entityMetadataList;
    }
}