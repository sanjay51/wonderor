import { Utils } from '../utils';
import { assertNotNull } from '@angular/compiler/src/output/output_ast';
import { Observable } from 'rxjs/internal/Observable';
import { API } from '../api/api';
import { HttpClient, HttpParams } from '@angular/common/http';

export class GetEntitiesByAuthorAPI extends API {
    public static API_NAME: string = "getEntitiesByAuthor";

    authorId: string;

    constructor(authorId: string) {
        super();
        this.authorId = authorId;
    }

    serve(http: HttpClient, URL: string): Observable<any> {
        let params = new HttpParams()
            .set("api", GetEntitiesByAuthorAPI.API_NAME)
            .set("authorId", this.authorId)

        return http.get(URL, {params: params});
    }

    validate() {
        Utils.assertNotNull(this.authorId, "authorId")
    }
}