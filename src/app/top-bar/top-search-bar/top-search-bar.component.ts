import { Component, OnInit } from '@angular/core';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { StateService } from '../../state.service';
import { ELEMENT_TOP_SEARCH_BAR } from '../../constants';

@Component({
  selector: ELEMENT_TOP_SEARCH_BAR,
  templateUrl: './top-search-bar.component.html',
  styleUrls: ['./top-search-bar.component.css']
})
export class TopSearchBarComponent implements OnInit {
  public faSearch = faSearch

  constructor(private state: StateService) { }

  ngOnInit() {
  }

  onClick() {
    this.state.activateSearchBar("")
  }

}
