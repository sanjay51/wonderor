import { ROUTE_PROFILE } from '../../sidebar/sidebar-option';
import { StateService } from '../../state.service';
import { AuthenticationService } from '../../user/authentication.service';
import { Component, OnInit } from '@angular/core';
import { faUserCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-top-profile-menu',
  templateUrl: './top-profile-menu.component.html',
  styleUrls: ['./top-profile-menu.component.css']
})
export class TopProfileMenuComponent implements OnInit {
  faUserCircle = faUserCircle
  
  constructor(public authentication: AuthenticationService, private state: StateService) { }

  ngOnInit() {
  }

  gotoProfile() {
    this.state.navigate(ROUTE_PROFILE, null);
  }

}
