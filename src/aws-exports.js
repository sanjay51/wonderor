// WARNING: DO NOT EDIT. This file is Auto-Generated by AWS Mobile Hub. It will be overwritten.

// Copyright 2017-2018 Amazon.com, Inc. or its affiliates (Amazon). All Rights Reserved.
// Code generated by AWS Mobile Hub. Amazon gives unlimited permission to
// copy, distribute and modify it.

// AWS Mobile Hub Project Constants
const awsmobile = {
    'aws_app_analytics': 'disable',
    'aws_cognito_identity_pool_id': 'us-west-2:85500ce9-3830-4209-95de-ec6800956a2c',
    'aws_cognito_region': 'us-west-2',
    'aws_content_delivery': 'enable',
    'aws_content_delivery_bucket': 'wonderor-hosting-mobilehub-870380696',
    'aws_content_delivery_bucket_region': 'us-west-2',
    'aws_content_delivery_cloudfront': 'enable',
    'aws_content_delivery_cloudfront_domain': 'd368dtuznffuzg.cloudfront.net',
    'aws_mobile_analytics_app_id': '99708d53625e40bd9fc523ad11f0f62f',
    'aws_mobile_analytics_app_region': 'us-east-1',
    'aws_project_id': 'bf3868d2-4ca9-400a-b784-aca02626d2f9',
    'aws_project_name': 'wonderor',
    'aws_project_region': 'us-west-2',
    'aws_resource_name_prefix': 'wonderor-mobilehub-870380696',
    'aws_sign_in_enabled': 'enable',
    'aws_user_pools': 'enable',
    'aws_user_pools_id': 'us-west-2_lE3nDbvZg',
    'aws_user_pools_web_client_id': '5po91up1omh3j9mu1l7jdg2h4o',
}

export default awsmobile;
